/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg01;

/**
 *
 * @author Mortti
 */
public class WaterDispenser {
    private int waterLevel;
    private int cup;
    private boolean powerOn;
    private boolean lidOn;
    
    
    public WaterDispenser (){
        this.waterLevel = 0;
        this.powerOn = false;
        this.lidOn = false;
        this.cup = 100;
    }
    
    
   public void waterButton(){
       if (this.powerOn){
           if (this.waterLevel >= cup){
               this.waterLevel = this.waterLevel - cup;
               System.out.println("Enjoy your water!");
       }
           else{
               System.out.println("Not enough water");
           }
           
   }
   }
   public void powerOnOff(){
       if (this.powerOn){
           this.powerOn = false;
       }
       else{
           this.powerOn = true;
           System.out.println("Power is on!");
       }    
   }
   public void fillTank(){
       this.lidOn = false;
       this.waterLevel = 10000;
       System.out.println("Tank is full");
       this.lidOn = true;
   }
   
   
}
